FROM node:10

RUN apt-get update && apt-get install -y vim

RUN mkdir -p /app/node_modules && chown -R node:node /app

ENV FORCE_COLOR=1

WORKDIR /app

COPY package*.json ./

USER node

RUN npm install --only=production

COPY --chown=node:node . .

EXPOSE 8080

CMD [ "npm", "start" ]