import { Terminal } from 'xterm'
import * as attach from 'xterm/dist/addons/attach/attach'
import * as fit from 'xterm/dist/addons/fit/fit'
import 'xterm/dist/xterm.css'
import './index.css'

Terminal.applyAddon(attach)
Terminal.applyAddon(fit)

const host = 'localhost'
const port = '8080'

const term = new Terminal({
  cursorBlink: true,
  theme: {
    foreground: `#cccccc`,
    background: `#0c0c0c`,
    cursor: `#cccccc`,
  },
})
term.open(document.getElementById('terminal'))
term.fit()
term.focus()
term.writeln(`Connecting to ${host}...`)

const cols = term.cols
const rows = term.rows

const socket = new WebSocket(`ws://${host}:${port}/?cols=${cols}&rows=${rows}`)

socket.onopen = () => {
  term.writeln('Connection established!')
  term.attach(socket)
}
socket.onclose = () => {
  term.writeln('Server disconnected!')
}
socket.onerror = () => {
  term.writeln('Server disconnected!')
}
