# Xterm-pty

Make your terminal accessible in browsers.

Xterm.js is a terminal emulator working in browsers. We can attach an instance of xterm.js to a pseudoterminal forked by node-pty via WebSocket. 

## Run

1. Make sure you have the latest Docker correctly installed.

2. Build static assets: `npm run build`

3. Build Docker image: `docker build -t <your username>/xterm-pty .`

4. Run Docker image: `docker run -p 8080:8080 -d <your username>/xterm-pty`

## Copyright

Copyright(c) 2019 Wentao Liu
