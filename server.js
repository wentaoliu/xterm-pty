const express = require('express')
const WebSocket = require('ws')
const http = require('http')
const url = require('url')

var pty = require('node-pty')

const port = parseInt(process.env.PORT, 10) || 8080

const app = express()

app.use(express.static('public'))

const server = http.createServer(app)
const wss = new WebSocket.Server({ server })

wss.on('connection', (ws, req) => {
  const params = url.parse(req.url, true).query;
  const cols = "cols" in params ? parseInt(params.cols.toString(), 10) : 80;
  const rows = "rows" in params ? parseInt(params.rows.toString(), 10) : 30; 

  var shell = pty.spawn('bash', ['-r'], {
    name: 'xterm',
    cwd: process.env.HOME,
    env: { 
      BASH: '/bin/bash',
      FORCE_COLOR: 1,
      HOME: '/home/node',
      PWD: '/home/node',
      SHELL: '/bin/bash',
      PATH: '/usr/sbin:/usr/bin:/sbin:/bin:/app/bin'
    },
    cols,
    rows,
  })

  shell.on('data', data => {
    ws.send(data)
  })

  ws.on('message', msg => {
    shell.write(msg)
  })
})

server.listen(port, () => {
  console.log(`Server started on port ${port}`)
})
